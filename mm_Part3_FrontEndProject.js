$('h2').text("Starting...");
// Initiate player and array of circles
var player = 0;
var cells = [
  [0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0],
  [0,0,0,0,0,0,0]
]

//Prompt players to input their names
var nameOne = prompt("Player One. Input your name. You will be in red")
var nameTwo = prompt("Player Two. Input your name. You will be in blue")


function setHeaderTurn(){
// player:
// 0 - Initiation
// 1 - player One
// 2 - player Two
// 31 - player One is the winner
// 32 - player Two is the winner
    if (player === 1){
      $('h2').text("Player "+ nameOne + " Turn:");
    }
    else if (player === 2) {
      $('h2').text("Player "+ nameTwo + " Turn:");
    }
    else if (player === 31) {
      $('h2').text("Player "+ nameOne + " is the Winner. To start a new game please refresh your browser");
    }
    else if (player === 32) {
      $('h2').text("Player "+ nameTwo + " is the Winner. To start a new game please refresh your browser");
    }
  }

function checkHorizontal(rowNum) {
  //TODO check if game is over in a new function
    var countInLine = 0; //length of sequence of cells with the same color
    var maxCountInLine = 0; //max length of sequence of cells with the same color
    var linePlayer  = 0; //player - owner of the same color sequence
    var maxLinePlayer = 0; //player - owner of the max of same color sequence

    for (var i = 0; i < cells.length; i++) {
      if ((cells[i][rowNum] > 0)&&(linePlayer ===  cells[i][rowNum])) {
        countInLine++;
      }
      else if (cells[i][rowNum] > 0) {
        linePlayer =  cells[i][rowNum];
        countInLine = 1;
      }
      else {
        countInLine = 0;
        linePlayer = 0;
      }
      if (countInLine > maxCountInLine) {
       maxCountInLine  = countInLine;
       maxLinePlayer = linePlayer;
      }
      if ( maxCountInLine >= 4) {
       console.log("WINNER!!!  "+ maxLinePlayer );
       player = 30 + maxLinePlayer;
      }
    }
    return 0; // no winner found
}

function checkVertical(columnNum) {

    var countInLine = 0; //length of sequence of cells with the same color
    var maxCountInLine = 0; //max length of sequence of cells with the same color
    var linePlayer  = 0; //player - owner of the same color sequence
    var maxLinePlayer = 0; //player - owner of the max of same color sequence

    for (var j = 0; j < cells[columnNum].length; j++) {
      if ((cells[columnNum][j] > 0)&&(linePlayer ===  cells[columnNum][j])) {
        countInLine++;
      }
      else if (cells[columnNum][j]> 0) {
        linePlayer =  cells[columnNum][j];
        countInLine = 1;
      }
      else {
        countInLine = 0;
        linePlayer = 0;
      }
      if (countInLine > maxCountInLine) {
       maxCountInLine  = countInLine;
       maxLinePlayer = linePlayer;
      }
      if ( maxCountInLine >= 4) {
       console.log("WINNER!!!  "+ maxLinePlayer );
       player = 30 + maxLinePlayer;
      }
    }
    return 0; // no winner found
}



function checkLeftDiagonal(rowNum,columnNum) {

  var countInLine = 0; //length of sequence of cells with the same color
  var maxCountInLine = 0; //max length of sequence of cells with the same color
  var linePlayer  = 0; //player - owner of the same color sequence
  var maxLinePlayer = 0; //player - owner of the max of same color sequence

  var startColumn = columnNum - rowNum;
  if (startColumn < 0) {
    startColumn = 0;
  }
  var endColumn = columnNum + (cells[columnNum].length-1 - rowNum);
  if (endColumn > cells.length-1) {
    endColumn = cells.length-1;
  }
  var startRow = rowNum - columnNum;
  if (startRow < 0) {
    startRow = 0;
  }
  var endRow = rowNum + (cells.length-1 - columnNum);
  if (endRow > cells[columnNum].length-1) {
    endRow = cells[columnNum].length-1;
  }
  for (var i = 0; i <= endColumn - startColumn; i++) {
    if ((cells[startColumn+i][startRow+i] > 0)&&(linePlayer ===  cells[startColumn+i][startRow+i])) {
      countInLine++;
    }
    else if (cells[startColumn+i][startRow+i] > 0) {
      linePlayer =  cells[startColumn+i][startRow+i] ;
      countInLine = 1;
    }
    else {
      countInLine = 0;
      linePlayer = 0;
    }
    if (countInLine > maxCountInLine) {
     maxCountInLine  = countInLine;
     maxLinePlayer = linePlayer;
    }
    if ( maxCountInLine >= 4) {
     player = 30 + maxLinePlayer;
    }
  }
  return 0; // no winner found
}

function checkRightDiagonal(rowNum,columnNum) {

  var countInLine = 0; //length of sequence of cells with the same color
  var maxCountInLine = 0; //max length of sequence of cells with the same color
  var linePlayer  = 0; //player - owner of the same color sequence
  var maxLinePlayer = 0; //player - owner of the max of same color sequence

  var startColumn = columnNum - (cells[columnNum].length-1 - rowNum);
  if (startColumn < 0) {
    startColumn = 0;
  }
  var endColumn = columnNum + rowNum;
  if (endColumn > cells.length-1) {
    endColumn = cells.length-1;
  }
  var startRow = rowNum - (cells.length-1 - columnNum);
  if (startRow < 0) {
    startRow = 0;
  }
  var endRow = rowNum + columnNum;
  if (endRow > cells[columnNum].length-1) {
    endRow = cells[columnNum].length-1;
  }
  for (var i = 0; i <= endColumn - startColumn; i++) {
    if ((cells[startColumn+i][endRow-i] > 0)&&(linePlayer ===  cells[startColumn+i][endRow-i])) {
      countInLine++;
    }
    else if (cells[startColumn+i][endRow-i] > 0) {
      linePlayer =  cells[startColumn+i][endRow-i] ;
      countInLine = 1;
    }
    else {
      countInLine = 0;
      linePlayer = 0;
    }
    if (countInLine > maxCountInLine) {
     maxCountInLine  = countInLine;
     maxLinePlayer = linePlayer;
    }
    if ( maxCountInLine >= 4) {
     console.log("WINNER!!!  "+ maxLinePlayer );
     player = 30 + maxLinePlayer;
    }
  }
  return 0; // no winner found
}


function gameLogic (el) {
  var columnNum = parseInt( $(el).parent().index() );
  //var clickedRowNum = parseInt( $(el).parent().parent().index() );

  //find row with gray cell which we will color.
  //it has max index of row and it is the lowes of the gray cells in the column
  var rowNum = -1; //row with gray cell which we will color
  for (var i = cells[columnNum].length-1; i >=0 ; i--) {
    if (cells[columnNum][i]===0) {
      rowNum = i;
      break;
    }
  }
  //set color on the cell according the Player
  if (rowNum >= 0) {
    if (player === 1) {
      cells[columnNum][rowNum] = 1;
      $("#table1 tr:eq("+rowNum +") td:eq("+columnNum+") div:eq(0)").attr('class','circle-red');
      player = 2
    }
    else if (player === 2) {
      cells[columnNum][rowNum] = 2;
      $("#table1 tr:eq("+rowNum +") td:eq("+columnNum+") div:eq(0)").attr('class','circle-blue');
      player = 1
    }
  }

  checkHorizontal(rowNum);
  if (player < 30){
    checkVertical(columnNum);
  }
  if (player < 30){
    checkLeftDiagonal(rowNum,columnNum);
  }
  if (player < 30){
    checkRightDiagonal(rowNum,columnNum);
  }
  setHeaderTurn();
}




//Start with player One and set header turn
player = 1;
setHeaderTurn();

//Treat each click with applying the logic of the game
$('td  > div').click(function() {
    gameLogic(this);
})
